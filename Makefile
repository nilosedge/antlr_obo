
all:
	java -jar antlr-4.6-complete.jar OBO.g4 -Dlanguage=Python2 -o output/

clean:
	rm -fr output

run:
	python run.py so.obo
