grammar OBO;

options {
  language=Python2;
}
	
prog: header EOL* entity* EOF;

header: (headerclause EOL*)+;
headerclause:
	headerkeyvaluepair
	| 'date' COLON dateTime
	| 'subsetdef' COLON ident quotedString
	| 'synonymtypedef' COLON ident quotedString SYNONYMSCOPE?
;

headerkeyvaluepair:
	headerkey COLON unquotedString;
	
headerkey:
	'format-version' | 'ontology' | 'data-version' | 'saved-by' | 'auto-generated-by' | 'remark' | 'UnreservedToken' | 'default-namespace';

//entity: term | typedef | instance;
entity: term | typedef;

term: '[Term]' EOL (termclause qualifierBlock? hiddencomment? EOL*)+;

termclause: 
		termkeyvaluepairTF
		| termkeyvaluepairID
		| termkeyvaluepairIDID
		| termkeyvaluepair
        | 'def' COLON quotedString xreflist
        | 'synonym' COLON quotedString SYNONYMSCOPE ident? xreflist
        | 'property_value' COLON ident ((quotedString xsdType) | ident) // property_value-Tag Relation-ID ( QuotedString XSD-Type | ID )
        | 'xref' COLON ident quotedString?
        | 'creation_date' COLON dateTimeISO8601
;

termkeyvaluepairTF:
	termkey COLON TF;
termkeyvaluepairID:
	termkey COLON ident;
termkeyvaluepairIDID:
	termkey COLON ident ident;
termkeyvaluepair:
	termkey COLON unquotedString;
	
termkey:
	'id' | 'name' | 'namespace' | 'comment' | 'created_by' | 'is_anonymous'
	| 'builtin' | 'alt_id' | 'consider' | 'is_obsolete'
	| 'subset' | 'union_of' | 'equivalent_to' | 'disjoint_from'
	| 'relationship' | 'replaced_by' | 'intersection_of' | 'is_a'
;
        

typedef: '[Typedef]' EOL (typedefclause qualifierBlock? hiddencomment? EOL*)+;

typedefclause:
		typedefkeyvaluepairTF
		| typedefkeyvaluepairID
		| typedefkeyvaluepairIDID
		| typedefkeyvaluepair
        | 'def' COLON quotedString xreflist
        | 'synonym' COLON quotedString SYNONYMSCOPE xreflist
        | 'property_value' COLON ident ((quotedString xsdType) | ident) // property_value-Tag Relation-ID ( QuotedString XSD-Type | ID )
        | 'creation_date' COLON dateTimeISO8601
        | 'xref' COLON ident quotedString?
        | 'expand_assertion_to' COLON quotedString xreflist
        | 'expand_expression_to' COLON quotedString xreflist
;
        
typedefkeyvaluepairTF:
	typedefkey COLON TF;
typedefkeyvaluepairID:
	typedefkey COLON ident;
typedefkeyvaluepairIDID:
	typedefkey COLON ident ident;
typedefkeyvaluepair:
	typedefkey COLON unquotedString;
    
typedefkey:
	'id' | 'is_anonymous' | 'name' | 'namespace' | 'alt_id' | 'comment' | 'subset'
	| 'domain' | 'range' | 'builtin' | 'holds_over_chain'
	| 'is_anti_symmetric' | 'is_cyclic' | 'is_reflexive' | 'is_symmetric'
	| 'is_transitive' | 'is_functional' | 'is_inverse_functional' | 'is-obsolete'
	| 'relationship' | 'replaced_by' | 'intersection_of' | 'is_a'
	| 'union_of' | 'equivalent_to' | 'disjoint_from' | 'inverse_of'
	| 'transitive_over' | 'equivalent_to_chain' | 'disjoint_over' | 'consider'
	| 'is_metadata_tag' | 'is_class_level_tag'
	| 'created_by' 
;
    
        
/*

instance: EOL+
        '[Instance]' EOL
        'id' RelationID EOL
        (instanceclause EOL)+;

instanceclause:
        'is_anonymous' COLON Boolean
        | 'name' COLON UnquotedString
        | 'namespace' COLON UnquotedString 
        | 'alt_id' ID 
        | 'def' QuotedString ID* 
        | 'comment' UnquotedString
        | 'subset' SubsetID 
        | 'synonym' QuotedString SynonymScope LB SynonymTypeID RB ID* 
        | 'xref' ID 
        | 'property_value' RelationID ID 
        | 'instance_of' ClassID 
        | 'relationship' RelationID ID 
        | 'created_by' COLON UnquotedString 
        | 'creation_date' dateTimeISO8601
        | 'is-obsolete' COLON Boolean
        | 'replaced_by' RelationID 
        | 'consider' ID;
*/


quotedString: '"' (~'"' | '\\"')* '"';
unquotedString: (~(EOL|EX))+;

SYNONYMSCOPE: 'EXACT' | 'BROAD' | 'NARROW' | 'RELATED';

TF: 'true' | 'false';

dateTimeISO8601: dateISO8601 timeISO8601;
dateISO8601: DIGIT DIGIT DIGIT DIGIT '-' DIGIT DIGIT '-' DIGIT DIGIT 'T';
timeISO8601: DIGIT DIGIT COLON DIGIT DIGIT COLON DIGIT DIGIT 'Z';

dateTime: date time;

date:
	DIGIT DIGIT '-' DIGIT DIGIT '-' DIGIT DIGIT DIGIT DIGIT
	| DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT DIGIT DIGIT
;
time: DIGIT DIGIT COLON DIGIT DIGIT;

synonymTypeID: ident;
subsetID: ident;
classID: ident;
relationID: ident;
instanceID: ident;

hiddencomment: EX unquotedString;

qualifierBlock: '{' qualifierList '}';
qualifierList: qualifier (COMMA qualifier)*;
qualifier: ident '=' quotedString;

commentList: '{' singleComment (COMMA singleComment)* '}';

singleComment: 'comment=' quotedString;

xreflist:
	LB xref? (COMMA xref)* RB;
	
xref: ident2 quotedString?;

ident2: nowscomma+;

xsdType: 'xsd:' ALPHACHAR+;

ident:
	ALPHACHAR ( '_' | ALPHACHAR)* COLON DIGIT*
	| ( nows* COLON nows* )
	| nowscolon*
	| 'url:'? ( 'http:' | 'https:' ) nows+;

obochar: SLASH (ALPHACHAR | COLON | COMMA | RB) | ~(EOL | SLASH);
nows: SLASH (ALPHACHAR | COLON | COMMA | RB) | ~(EOL | SLASH | WS | QUOTE);
nowscolon: SLASH (ALPHACHAR | COLON | COMMA | RB) | ~(EOL | SLASH | WS | COLON | QUOTE);
nowscomma: SLASH (ALPHACHAR | COLON | COMMA | RB) | ~(EOL | SLASH | WS | COMMA | QUOTE);

ALPHACHAR: [a-zA-Z];
DIGIT: [0-9];

COLON: ':';
EX: '!';
COMMA: ',';
QUOTE: '"';

LB: '[';
RB: ']';

SLASH: '\\' | '\\\\\\';

WS:
	[ \t]+ -> channel(HIDDEN);

EOL:
	'\r'?'\n';

ANYCHAR: .;