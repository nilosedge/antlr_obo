import sys
from antlr4 import *
from output.OBOLexer import OBOLexer
from output.OBOParser import OBOParser

def main(argv):
    input = FileStream(argv[1], 'utf-8')
    lexer = OBOLexer(input)
    #lexer._listeners = [ MyErrorListener() ]
    stream = CommonTokenStream(lexer)
    parser = OBOParser(stream)
    #parser.addErrorListener(MyErrorListener())
    #parser._listeners = [ MyErrorListener() ]
    tree = parser.prog()

if __name__ == '__main__':
    main(sys.argv)
